﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    FirstName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Preferences",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferences", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerPreferences",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    CustomerId = table.Column<Guid>(type: "TEXT", nullable: false),
                    PreferenceId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPreferences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerPreferences_Preferences_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    FirstName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    RoleId = table.Column<Guid>(type: "TEXT", nullable: false),
                    AppliedPromocodesCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PromoCodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Code = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    ServiceInfo = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    BeginDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    EndDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    PartnerManagerId = table.Column<Guid>(type: "TEXT", nullable: false),
                    PreferenceId = table.Column<Guid>(type: "TEXT", nullable: false),
                    CustomerId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Employees_PartnerManagerId",
                        column: x => x.PartnerManagerId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PromoCodes_Preferences_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "Preferences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), "ivan_sergeev@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"), "sobaka@yandex.ru", "Олег", "Зайцев" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("33f10f46-7228-4c88-a0c5-5967fa55e9c5"), "Vasiz@gmail.com", "Василий", "Петин" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("2e6f02d6-fe82-40e4-b1ab-34e872989b5a"), "GRDCool@hitler.hell", "Кирилл", "Ладин" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "Театр" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "Семья" });

            migrationBuilder.InsertData(
                table: "Preferences",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "Дети" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager" });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("01dcb1b6-20ef-48d5-aefc-68195e5d622b"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("d688d697-09b9-466b-a142-d11932fb7beb"), new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("f313483a-a185-4c10-a500-c4305130bd79"), new Guid("33f10f46-7228-4c88-a0c5-5967fa55e9c5"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("47ed481c-72f5-4a18-9be5-7250b788cc12"), new Guid("2e6f02d6-fe82-40e4-b1ab-34e872989b5a"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "CustomerPreferences",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("669319c5-7098-4177-af7f-6a74d2071a37"), new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId" },
                values: new object[] { new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), 5, "owner@somemail.ru", "Иван", "Сергеев", new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02") });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId" },
                values: new object[] { new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), 10, "andreev@somemail.ru", "Петр", "Андреев", new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665") });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("95b6c98c-702a-4e13-b8fd-324c63fa27ab"), new DateTime(2023, 9, 28, 17, 37, 22, 226, DateTimeKind.Local).AddTicks(9384), "asasas", new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new DateTime(2023, 10, 3, 17, 37, 22, 227, DateTimeKind.Local).AddTicks(8343), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "Иван Сергеев", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "Some service info" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("a08c9079-217a-4eb9-83f1-54d4c01b9d80"), new DateTime(2023, 9, 29, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1323), "zxzxzxzx", new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"), new DateTime(2023, 10, 1, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1344), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "Иван Сергеев", new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "Some service info" });

            migrationBuilder.InsertData(
                table: "PromoCodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("c88de4c9-9dc2-48a3-ad61-587005de33e0"), new DateTime(2023, 10, 2, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1427), "cvcvcvcv", new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"), new DateTime(2023, 10, 5, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1428), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "Иван Сергеев", new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "Some service info" });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreferences_CustomerId",
                table: "CustomerPreferences",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreferences_PreferenceId",
                table: "CustomerPreferences",
                column: "PreferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_RoleId",
                table: "Employees",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_CustomerId",
                table: "PromoCodes",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_PartnerManagerId",
                table: "PromoCodes",
                column: "PartnerManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_PreferenceId",
                table: "PromoCodes",
                column: "PreferenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerPreferences");

            migrationBuilder.DropTable(
                name: "PromoCodes");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Preferences");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class PhoneAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Customers",
                type: "TEXT",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("2e6f02d6-fe82-40e4-b1ab-34e872989b5a"),
                column: "Phone",
                value: "+74");

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("33f10f46-7228-4c88-a0c5-5967fa55e9c5"),
                column: "Phone",
                value: "+73");

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                column: "Phone",
                value: "+71");

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"),
                column: "Phone",
                value: "+72");

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("95b6c98c-702a-4e13-b8fd-324c63fa27ab"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 9, 28, 17, 48, 36, 262, DateTimeKind.Local).AddTicks(7585), new DateTime(2023, 10, 3, 17, 48, 36, 263, DateTimeKind.Local).AddTicks(6661) });

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("a08c9079-217a-4eb9-83f1-54d4c01b9d80"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 9, 29, 17, 48, 36, 263, DateTimeKind.Local).AddTicks(9582), new DateTime(2023, 10, 1, 17, 48, 36, 263, DateTimeKind.Local).AddTicks(9613) });

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("c88de4c9-9dc2-48a3-ad61-587005de33e0"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 10, 2, 17, 48, 36, 263, DateTimeKind.Local).AddTicks(9686), new DateTime(2023, 10, 5, 17, 48, 36, 263, DateTimeKind.Local).AddTicks(9688) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Customers");

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("95b6c98c-702a-4e13-b8fd-324c63fa27ab"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 9, 28, 17, 37, 22, 226, DateTimeKind.Local).AddTicks(9384), new DateTime(2023, 10, 3, 17, 37, 22, 227, DateTimeKind.Local).AddTicks(8343) });

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("a08c9079-217a-4eb9-83f1-54d4c01b9d80"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 9, 29, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1323), new DateTime(2023, 10, 1, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1344) });

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("c88de4c9-9dc2-48a3-ad61-587005de33e0"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 10, 2, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1427), new DateTime(2023, 10, 5, 17, 37, 22, 228, DateTimeKind.Local).AddTicks(1428) });
        }
    }
}

﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class CustomerModelEntityMappingProfile : Profile
    {
        public CustomerModelEntityMappingProfile()
        {
            CreateMap<Customer, CustomerResponse>()
                .ForMember(d => d.Preferences, opt => opt.Ignore())
                .AfterMap((customer, customerResponse, context) =>
                {
                    var preferencesEntities = customer.CustomerPreferences
                        .Select(cp => cp.Preference);
                    var preferencesModels = context.Mapper.Map<List<PreferenceResponse>>(preferencesEntities);
                    customerResponse.Preferences = preferencesModels;
                });
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(d => d.Id, opt => opt.MapFrom(cr => Guid.Empty))
                .ForMember(d => d.PromoCodes, opt => opt.MapFrom(cr => new List<PromoCode>()))
                .ForMember(d => d.CustomerPreferences, opt => opt.MapFrom(cr => new List<CustomerPreference>()));
        }
    }
}

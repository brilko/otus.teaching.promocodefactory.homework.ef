﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.DTOs;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PromoCodeModelEntityMappingProfile : Profile
    {
        public PromoCodeModelEntityMappingProfile()
        {
            CreateMap<PromoCode, PromoCodeShortResponse>()
                .ForMember(d => d.BeginDate, opt => opt.MapFrom(pc => pc.BeginDate.ToLongDateString()))
                .ForMember(d => d.EndDate, opt => opt.MapFrom(pc => pc.EndDate.ToLongDateString()))
                .ForMember(d => d.PreferenceName, opt => opt.MapFrom(pc => pc.Preference.Name))
                .ForMember(d => d.CustomerFullName, opt => opt.MapFrom(pc => pc.Customer.FullName));
            CreateMap<GivePromoCodeRequestModel, GivePromoCodeRequestDto>();
        }
    }
}

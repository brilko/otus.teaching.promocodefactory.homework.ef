﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class RoleModelEntityMappingProfile : Profile
    {
        public RoleModelEntityMappingProfile()
        {
            CreateMap<Role, RoleItemResponse>();
        }
    }
}

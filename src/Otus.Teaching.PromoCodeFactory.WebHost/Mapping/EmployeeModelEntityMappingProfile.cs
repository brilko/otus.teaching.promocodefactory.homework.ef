﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class EmployeeModelEntityMappingProfile : Profile
    {
        public EmployeeModelEntityMappingProfile()
        {
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Employee, EmployeeResponse>();
        }
    }
}

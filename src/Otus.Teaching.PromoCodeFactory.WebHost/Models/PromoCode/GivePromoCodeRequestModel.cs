﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode
{
    public class GivePromoCodeRequestModel
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public string PreferenceName { get; set; }
    }
}
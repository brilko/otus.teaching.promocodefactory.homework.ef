﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Services.Implementations;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Registrar
    {
        public static IServiceCollection AddCustomerServicesExtension(
            this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .InstallDataBase(configuration)
                .AddRepositories()
                .InstallMappers()
                .InstallServices();
        }

        private static IServiceCollection InstallDataBase(
            this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetValue<string>("ConnectionString");
            return services.AddDbContext<DatabaseContext>(optionsBuilder =>
                optionsBuilder
                    .UseSqlite(connectionString, 
                        b => b.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.WebHost")),
                    ServiceLifetime.Transient);
        }

        private static IServiceCollection AddEfRepository<T>(this IServiceCollection services)
            where T : BaseEntity
        {
            return services.AddScoped<IRepository<T>, EfRepository<T>>();
        }

        private static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services
                .AddEfRepository<Employee>()
                .AddEfRepository<Customer>()
                .AddEfRepository<Role>()
                .AddEfRepository<PromoCode>()
                .AddEfRepository<CustomerPreference>()
                .AddEfRepository<Preference>();
        }

        private static IServiceCollection InstallMappers(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CustomerModelEntityMappingProfile>();
                cfg.AddProfile<EmployeeModelEntityMappingProfile>();
                cfg.AddProfile<PromoCodeModelEntityMappingProfile>();
                cfg.AddProfile<RoleModelEntityMappingProfile>();
                cfg.AddProfile<PreferenceModelMappingProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return services.AddSingleton<IMapper>(new Mapper(configuration));
        }

        private static IServiceCollection InstallServices(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<ICustomerPreferenceService, CustomerPreferenceService>()
                .AddScoped<IPromocodeService, PromocodeService>()
                .AddScoped<IEmployeeService, EmployeeService>()
                .AddScoped<IPreferenceService, PreferenceService>()
                .AddScoped<ICustomerIdFinderByPreferenceId, CustomerPreferenceService>();
        }
    }
}

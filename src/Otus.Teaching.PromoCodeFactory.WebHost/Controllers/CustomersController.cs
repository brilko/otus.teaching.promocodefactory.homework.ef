﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    public class CustomersController
        : AbstractController<Customer, CustomerShortResponse>
    {
        private readonly ICustomerService customerService;

        public CustomersController(
            IRepository<Customer> customerRepo,
            IMapper mapper,
            ICustomerService customerService) : base(customerRepo, mapper)
        {
            this.customerService = customerService;
        }

        /// <summary>
        /// Получить одного пользователя по id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerByIdAsync(Guid id)
        {
            var customerEntity = await repository.GetByIdAsync(id);
            if (customerEntity == null)
                return NotFound();
            var customerModel = mapper.Map<CustomerResponse>(customerEntity);
            return Ok(customerModel);
        }

        /// <summary>
        /// Создать нового пользователя
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = mapper.Map<Customer>(request);
            var guid = await customerService.CreateCustomerAsync(customer, request.PreferenceIds);
            return Ok(guid);
        }

        /// <summary>
        /// Изменить данные существующего пользователя
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = mapper.Map<Customer>(request);
            customer.Id = id;
            var isUpdated = await customerService.TryEditCustomersAsync(customer, request.PreferenceIds);
            if (isUpdated)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// Удалить клиента по его id
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var isDeleted = await customerService.TryDeleteCustomerAndPromocodesAsync(id);
            if (isDeleted)
                return Ok();
            return NotFound();
        }
    }
}
﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    public class EmployeesController
        : AbstractController<Employee, EmployeeShortResponse>
    {

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
            :base(employeeRepository, mapper) { }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await repository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }
    }
}
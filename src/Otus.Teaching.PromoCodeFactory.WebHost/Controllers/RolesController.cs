﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли
    /// </summary>
    public class RolesController : AbstractController<Role, RoleItemResponse>
    {
        public RolesController(IRepository<Role> repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}
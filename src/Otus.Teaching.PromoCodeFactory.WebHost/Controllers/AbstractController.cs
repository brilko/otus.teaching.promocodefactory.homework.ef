﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public abstract class AbstractController<T, TGetAll> : ControllerBase
        where T : BaseEntity
    {
        protected readonly IRepository<T> repository;
        protected readonly IMapper mapper;

        public AbstractController(IRepository<T> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Получить все. 
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<TGetAll>> GetAllAsync()
        {
            var entities = await repository.GetAllAsync();

            var models = mapper.Map<IEnumerable<TGetAll>>(entities);

            return models;
        }
    }
}

﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class PreferenceController : AbstractController<Preference, PreferenceResponse>
    {
        public PreferenceController(IRepository<Preference> repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}

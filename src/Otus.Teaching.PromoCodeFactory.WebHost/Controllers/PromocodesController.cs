﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.DTOs;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    public class PromocodesController
        : AbstractController<PromoCode, PromoCodeShortResponse>
    {
        private readonly IPromocodeService service;
        public PromocodesController(IRepository<PromoCode> promocodeRepo, IMapper mapper, IPromocodeService service)
            :base(promocodeRepo, mapper) 
        {
            this.service = service;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequestModel request)
        {
            var dto = mapper.Map<GivePromoCodeRequestDto>(request);
            var result = await service.GivePromocodeToEveryCustomerWithThisPreferenceAsync(dto);
            return result switch
            {
                IPromocodeService.PromoCodeGivingAnswers.PartnerDoesntExist 
                    => NotFound("Partner with name " + request.PartnerName + " doesn`t exist."),
                IPromocodeService.PromoCodeGivingAnswers.PreferenceDoesntExist 
                    => NotFound("Preference " + request.PreferenceName + " doen`t exist."),
                IPromocodeService.PromoCodeGivingAnswers.ThereIsntAnyCustomerWithThatPreference
                    => NotFound("There isn`t any customer with that preference."),
                IPromocodeService.PromoCodeGivingAnswers.PromocodesWereGiven => Ok(),
                _ => throw new Exception(),
            };
        }
    }
}
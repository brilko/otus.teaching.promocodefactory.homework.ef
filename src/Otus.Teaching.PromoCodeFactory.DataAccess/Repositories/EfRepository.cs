﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly DbSet<T> set;
        private readonly DbContext context;

        public EfRepository(DatabaseContext context)
        {
            this.context = context;
            set = context.Set<T>();
        }

        public async Task CreateRangeAssync(IEnumerable<T> entities)
        {
            await set.AddRangeAsync(entities);
            await context.SaveChangesAsync();
        }

        public async Task<Guid> CreateAssync(T entity)
        {
            var trackingEntity = await set.AddAsync(entity);
            await context.SaveChangesAsync();
            trackingEntity.State = EntityState.Detached;
            return trackingEntity.Entity.Id;
        }

        public async Task<bool> TryDeleteAssync(Guid id)
        {
            return await Task.Run(() =>
            {
                var entity = set.Find(id);
                if (entity == null)
                    return false;
                set.Remove(entity);
                context.SaveChanges();
                context.Entry(entity).State = EntityState.Detached;
                return true;
            });
        }

        public IQueryable<T> GetByLinq()
        {
            return set;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await set.ToArrayAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdAssync(IEnumerable<Guid> ids)
        {
            return await set
                .Where(e =>
                    ids.Any(id => e.Id == id))
                .ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await set.FindAsync(id);
            return entity;
        }

        public async Task<bool> TryUpdateAssync(T entity)
        {
            return await Task.Run(() =>
            {
                if (!set.Any(e => e.Id == entity.Id))
                    return false;
                set.Update(entity);
                context.SaveChanges();
                return true;
            });
        }

        public async Task<bool> IsExistByIdAssync(Guid id)
        {
            return await set.AnyAsync(e => e.Id == id);
        }

        public async Task DeleteRangeAsync(IEnumerable<Guid> ids)
        {
            await Task.Run(() =>
            {
                var entities = set.Where(e => ids.Any(id => e.Id == id));
                set.RemoveRange(entities);
                context.SaveChanges();
            });
        }
    }
}

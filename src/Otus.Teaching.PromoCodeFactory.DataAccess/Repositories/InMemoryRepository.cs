﻿//using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
//using Otus.Teaching.PromoCodeFactory.Core.Domain;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
//{
//    public class InMemoryRepository<T>
//        : IRepository<T>
//        where T : BaseEntity
//    {
//        protected IEnumerable<T> Data { get; set; }

//        public InMemoryRepository(IEnumerable<T> data)
//        {
//            Data = data;
//        }

//        #region Get
//        public Task<IEnumerable<T>> GetAllAsync()
//        {
//            return Task.FromResult(Data);
//        }

//        public Task<T> GetByIdAsync(Guid id)
//        {
//            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
//        }

//        public Task<IEnumerable<T>> GetRangeByIdAssync(IEnumerable<Guid> ids)
//        {
//            throw new NotImplementedException();
//        }

//        public IQueryable<T> GetByLinq(Func<IQueryable<T>, IQueryable<T>> func)
//        {
//            throw new NotImplementedException();
//        }
//        #endregion Get

//        #region Create
//        public Task<Guid> CreateAssync(T entity)
//        {
//            throw new NotImplementedException();
//        }

//        public Task CreateRangeAssync(IEnumerable<T> entities)
//        {
//            throw new NotImplementedException();
//        }
//        #endregion Create

//        #region Update
//        public Task<bool> TryUpdateAssync(T entity)
//        {
//            throw new NotImplementedException();
//        }
//        #endregion Update

//        #region Delete
//        public Task<bool> TryDeleteAssync(Guid id)
//        {
//            throw new NotImplementedException();
//        }

//        public Task DeleteRangeAsync(IEnumerable<Guid> ids)
//        {
//            throw new NotImplementedException();
//        }
//        #endregion Delete

//        #region isExist
//        public Task<bool> IsExistByIdAssync(Guid id)
//        {
//            throw new NotImplementedException();
//        }

//        public Task<IEnumerable<T>> GetRangeByIdAssync()
//        {
//            throw new NotImplementedException();
//        }

//        public IQueryable<T> GetByLinq()
//        {
//            throw new NotImplementedException();
//        }
//        #endregion IsExist
//    }
//}
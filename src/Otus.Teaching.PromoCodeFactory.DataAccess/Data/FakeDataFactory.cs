﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        #region Roles
        private static readonly Role roleAdmin = new Role()
        {
            Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
            Name = "Admin",
            Description = "Администратор",
        };
        private static readonly Role rolePartnerManager = new Role()
        {
            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
            Name = "PartnerManager",
            Description = "Партнерский менеджер"
        };
        public static IEnumerable<Role> Roles = new List<Role>()
        {
            roleAdmin,
            rolePartnerManager
        };
        #endregion

        #region Employees
        private static readonly Employee employeeAdmin = new Employee()
        {
            Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
            Email = "owner@somemail.ru",
            FirstName = "Иван",
            LastName = "Сергеев",
            RoleId = roleAdmin.Id,
            AppliedPromocodesCount = 5
        };
        public static IEnumerable<Employee> Employees = new List<Employee>()
        {
            employeeAdmin,
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = rolePartnerManager.Id,
                AppliedPromocodesCount = 10
            },
        };
        #endregion

        #region Preferences
        private static readonly Preference preferenceTheater = new Preference()
        {
            Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
            Name = "Театр",
        };
        private static readonly Preference preferenceFamily = new Preference()
        {
            Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
            Name = "Семья",
        };
        private static readonly Preference preferenceKids = new Preference()
        {
            Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
            Name = "Дети",
        };
        public static IEnumerable<Preference> Preferences = new List<Preference>()
        {
            preferenceTheater,
            preferenceFamily,
            preferenceKids
        };
        #endregion

        #region Customers
        private static readonly Customer customerIvanPetrov = new Customer()
        {
            Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
            Email = "ivan_sergeev@mail.ru",
            FirstName = "Иван",
            LastName = "Петров",
            Phone = "+71"
        };
        private static readonly Customer customerOlegZaicev = new Customer()
        {
            Id = Guid.Parse("a8debc6c-1f0f-46c6-a03b-5fc76fb233f0"),
            Email = "sobaka@yandex.ru",
            FirstName = "Олег",
            LastName = "Зайцев",
            Phone = "+72"
        };
        private static readonly Customer customerVasiliiPetin = new Customer()
        {
            Id = Guid.Parse("33f10f46-7228-4c88-a0c5-5967fa55e9c5"),
            Email = "Vasiz@gmail.com",
            FirstName = "Василий",
            LastName = "Петин",
            Phone = "+73"
        };
        private static readonly Customer customerKirillLadin = new Customer() 
        {
            Id = Guid.Parse("2e6f02d6-fe82-40e4-b1ab-34e872989b5a"),
            Email = "GRDCool@hitler.hell",
            FirstName = "Кирилл",
            LastName = "Ладин",
            Phone = "+74"
        };
        public static IEnumerable<Customer> Customers = new List<Customer>()
        {
            customerIvanPetrov,
            customerOlegZaicev,
            customerVasiliiPetin,
            customerKirillLadin
        };
        #endregion Customers

        #region PromoCodes
        public static IEnumerable<PromoCode> PromoCodes = new List<PromoCode>()
        {
            new PromoCode()
            {
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(5),
                Code = "asasas",
                CustomerId = customerIvanPetrov.Id,
                Id = Guid.Parse("95b6c98c-702a-4e13-b8fd-324c63fa27ab"),
                PartnerManagerId = employeeAdmin.Id,
                PartnerName = employeeAdmin.FullName,
                ServiceInfo = "Some service info",
                PreferenceId = preferenceTheater.Id
            },
            new PromoCode()
            {
                BeginDate = DateTime.Now.AddDays(1),
                EndDate = DateTime.Now.AddDays(3),
                Code = "zxzxzxzx",
                CustomerId = customerOlegZaicev.Id,
                Id = Guid.Parse("a08c9079-217a-4eb9-83f1-54d4c01b9d80"),
                PartnerManagerId = employeeAdmin.Id,
                PartnerName = employeeAdmin.FullName,
                ServiceInfo = "Some service info",
                PreferenceId = preferenceKids.Id
            },
            new PromoCode()
            {
                BeginDate = DateTime.Now.AddDays(4),
                EndDate = DateTime.Now.AddDays(7),
                Code = "cvcvcvcv",
                CustomerId = customerOlegZaicev.Id,
                Id = Guid.Parse("c88de4c9-9dc2-48a3-ad61-587005de33e0"),
                PartnerManagerId = employeeAdmin.Id,
                PartnerName = employeeAdmin.FullName,
                ServiceInfo = "Some service info",
                PreferenceId = preferenceFamily.Id
            }
        };
        #endregion

        #region CustomersPreferences
        public static IEnumerable<CustomerPreference> CustomerPreferences = new List<CustomerPreference>()
        {
            new CustomerPreference()
            {
                Id = Guid.Parse("01dcb1b6-20ef-48d5-aefc-68195e5d622b"),
                CustomerId = customerIvanPetrov.Id,
                PreferenceId = preferenceTheater.Id
            },
            new CustomerPreference()
            {
                Id = Guid.Parse("d688d697-09b9-466b-a142-d11932fb7beb"),
                CustomerId = customerOlegZaicev.Id,
                PreferenceId = preferenceFamily.Id
            },
            new CustomerPreference()
            {
                Id = Guid.Parse("669319c5-7098-4177-af7f-6a74d2071a37"),
                CustomerId = customerOlegZaicev.Id,
                PreferenceId = preferenceKids.Id
            },
            new CustomerPreference()
            {
                Id = Guid.Parse("f313483a-a185-4c10-a500-c4305130bd79"),
                CustomerId = customerVasiliiPetin.Id,
                PreferenceId = preferenceFamily.Id
            },
            new CustomerPreference()
            {
                Id = Guid.Parse("47ed481c-72f5-4a18-9be5-7250b788cc12"),
                CustomerId = customerKirillLadin.Id,
                PreferenceId = preferenceFamily.Id
            },
        };
        #endregion
    }
}


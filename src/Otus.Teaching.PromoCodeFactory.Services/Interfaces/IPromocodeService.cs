﻿using Otus.Teaching.PromoCodeFactory.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface IPromocodeService
    {
        public Task DeleteCustomerPromocodes(Guid customerId);

        public Task<PromoCodeGivingAnswers> GivePromocodeToEveryCustomerWithThisPreferenceAsync
            (GivePromoCodeRequestDto promoCodeRequest);

        public enum PromoCodeGivingAnswers 
        {
            PartnerDoesntExist,
            PreferenceDoesntExist,
            ThereIsntAnyCustomerWithThatPreference,
            PromocodesWereGiven
        }
    }
}

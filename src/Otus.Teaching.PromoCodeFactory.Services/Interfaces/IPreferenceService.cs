﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface IPreferenceService
    {
        Task<Guid> GetGuidByName(string name);
    }
}

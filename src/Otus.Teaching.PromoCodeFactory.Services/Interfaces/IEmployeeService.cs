﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<Guid> GetGuidByName(string fullName); 
    }
}

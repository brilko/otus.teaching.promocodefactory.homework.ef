﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface ICustomerIdFinderByPreferenceId
    {
        Task<IEnumerable<Guid>> FindCustomerIdsByPreferenceId(Guid preferenceId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface ICustomerPreferenceService
    {
        public Task AddCustomersPreferencesAsync(Guid customerId, IEnumerable<Guid> preferenceIds);
        public Task UpdateCustomersPreferencesArangeAsync(Guid customerId, IEnumerable<Guid> preferenceIds);
    }
}

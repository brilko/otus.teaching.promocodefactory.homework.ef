﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Interfaces
{
    public interface ICustomerService
    {
        public Task<Guid> CreateCustomerAsync(Customer customer, IEnumerable<Guid> preferenceIds);

        public Task<bool> TryEditCustomersAsync(Customer customer, IEnumerable<Guid> preferenceIds);

        public Task<bool> TryDeleteCustomerAndPromocodesAsync(Guid customersId);
    }
}

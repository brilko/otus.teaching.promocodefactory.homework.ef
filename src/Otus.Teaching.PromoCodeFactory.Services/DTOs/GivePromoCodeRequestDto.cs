﻿namespace Otus.Teaching.PromoCodeFactory.Services.DTOs
{
    public class GivePromoCodeRequestDto
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public string PreferenceName { get; set; }
    }
}

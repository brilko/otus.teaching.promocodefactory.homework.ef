﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.DTOs;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Otus.Teaching.PromoCodeFactory.Services.Interfaces.IPromocodeService;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class PromocodeService : IPromocodeService
    {
        private readonly IRepository<PromoCode> repository;
        private readonly IEmployeeService employeeService;
        private readonly IPreferenceService preferenceService;
        private readonly int addDaysToBeginDate = 0;
        private readonly int addDaysToEndDate = 5;
        private readonly ICustomerIdFinderByPreferenceId customerFinder;
        public PromocodeService(IRepository<PromoCode> repository,
            IEmployeeService employeeService,
            IPreferenceService preferenceService,
            ICustomerIdFinderByPreferenceId customerFinder)
        {
            this.repository = repository;
            this.employeeService = employeeService;
            this.preferenceService = preferenceService;
            this.customerFinder = customerFinder;   
        }
        public async Task DeleteCustomerPromocodes(Guid customerId)
        {
            var promocodeIds = await repository
                .GetByLinq()
                .Where(pc => pc.CustomerId == customerId)
                .Select(pc => pc.Id)
                .ToListAsync();
            await repository.DeleteRangeAsync(promocodeIds);
        }

        private PromoCode CreateNewPromocode(GivePromoCodeRequestDto promoCodeRequest, Guid partnerId,
            Guid preferenceId, Guid customerId) 
        {
            return new PromoCode() 
            {
                Id = Guid.Empty,
                Code = promoCodeRequest.PromoCode,
                ServiceInfo = promoCodeRequest.ServiceInfo,
                PartnerManagerId = partnerId,
                PartnerName = promoCodeRequest.PartnerName,
                PreferenceId = preferenceId,
                BeginDate = DateTime.Now.AddDays(addDaysToBeginDate),
                EndDate = DateTime.Now.AddDays(addDaysToEndDate),
                CustomerId = customerId,
            };
        }

        public async Task<PromoCodeGivingAnswers> GivePromocodeToEveryCustomerWithThisPreferenceAsync(
            GivePromoCodeRequestDto promoCodeRequest)
        {
            var partnerId = await employeeService.GetGuidByName(promoCodeRequest.PartnerName);
            if (partnerId == Guid.Empty)
                return PromoCodeGivingAnswers.PartnerDoesntExist;

            var preferenceId = await preferenceService.GetGuidByName(promoCodeRequest.PreferenceName);
            if (preferenceId == Guid.Empty)
                return PromoCodeGivingAnswers.PreferenceDoesntExist;

            var customersIds = await customerFinder.FindCustomerIdsByPreferenceId(preferenceId);

            var promocodes = customersIds
                .Select(customerId => 
                    CreateNewPromocode(promoCodeRequest, partnerId, preferenceId, customerId));

            await repository.CreateRangeAssync(promocodes);
            return PromoCodeGivingAnswers.PromocodesWereGiven;
        }
    }
}

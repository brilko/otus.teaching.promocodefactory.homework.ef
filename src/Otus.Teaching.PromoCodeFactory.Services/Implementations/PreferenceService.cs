﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IRepository<Preference> repository;

        public PreferenceService(IRepository<Preference> repository)
        {
            this.repository = repository;
        }

        public async Task<Guid> GetGuidByName(string name)
        {
            return await repository
                .GetByLinq()
                .Where(preference => preference.Name == name)
                .Select(p => p.Id)
                .FirstOrDefaultAsync();
        }
    }
}

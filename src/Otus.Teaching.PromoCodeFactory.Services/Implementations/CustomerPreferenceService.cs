﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class CustomerPreferenceService : ICustomerPreferenceService, ICustomerIdFinderByPreferenceId
    {
        private readonly IRepository<CustomerPreference> customerPreferenceRepository;

        public CustomerPreferenceService(IRepository<CustomerPreference> customerPreferenceRepository)
        {
            this.customerPreferenceRepository = customerPreferenceRepository;
        }

        public async Task AddCustomersPreferencesAsync(Guid customerId, IEnumerable<Guid> preferenceIds)
        {
            await customerPreferenceRepository.CreateRangeAssync(preferenceIds
                .Select(pid => new CustomerPreference()
                {
                    CustomerId = customerId,
                    PreferenceId = pid
                }));
        }

        public async Task<IEnumerable<Guid>> FindCustomerIdsByPreferenceId(Guid preferenceId)
        {
            return await customerPreferenceRepository
                .GetByLinq()
                .Where(cp => cp.PreferenceId == preferenceId)
                .Select(cp => cp.Customer.Id)
                .ToListAsync();
        }

        public async Task UpdateCustomersPreferencesArangeAsync(Guid customerId, IEnumerable<Guid> preferenceIds)
        {
            var customerPreferences = await customerPreferenceRepository
                .GetByLinq()
                .Where(cp => cp.CustomerId == customerId)
                .ToListAsync();
            var intersection = customerPreferences
                .Select(cp => cp.Id)
                .Intersect(preferenceIds);
            var toDeleteCustomersPreferenceIds = customerPreferences
                .Where(cp => intersection.All(inter => inter != cp.Id))
                .Select(cp => cp.Id);
            var toCreatePrefenceIds = preferenceIds
                .Except(intersection);
            await customerPreferenceRepository.DeleteRangeAsync(toDeleteCustomersPreferenceIds);
            await AddCustomersPreferencesAsync(customerId, toCreatePrefenceIds);
        }
    }
}

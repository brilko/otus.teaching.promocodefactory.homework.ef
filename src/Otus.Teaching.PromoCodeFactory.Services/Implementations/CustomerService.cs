﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> customerRepo;
        private readonly ICustomerPreferenceService customerPreferenceService;
        private readonly IPromocodeService promocodeService;
        public CustomerService(
            IRepository<Customer> customerRepo,
            ICustomerPreferenceService customerPreferenceService,
            IPromocodeService promocodeService)
        {
            this.customerRepo = customerRepo;
            this.customerPreferenceService = customerPreferenceService;
            this.promocodeService = promocodeService;
        }

        public async Task<Guid> CreateCustomerAsync(Customer customer, IEnumerable<Guid> preferenceIds)
        {
            customer.Id = await customerRepo.CreateAssync(customer);
            await customerPreferenceService.AddCustomersPreferencesAsync(customer.Id, preferenceIds);
            return customer.Id;
        }

        public async Task<bool> TryDeleteCustomerAndPromocodesAsync(Guid customersId)
        {
            await promocodeService.DeleteCustomerPromocodes(customersId);
            var isDeleted = await customerRepo.TryDeleteAssync(customersId);
            return isDeleted;
        }

        public async Task<bool> TryEditCustomersAsync(Customer customer, IEnumerable<Guid> preferenceIds)
        {
            if (!await customerRepo.TryUpdateAssync(customer))
                return false;
            await customerPreferenceService.UpdateCustomersPreferencesArangeAsync(customer.Id, preferenceIds);
            return true;
        }

    }
}

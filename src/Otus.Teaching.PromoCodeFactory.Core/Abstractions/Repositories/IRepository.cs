﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        #region Get
        IQueryable<T> GetByLinq();

        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetRangeByIdAssync(IEnumerable<Guid> ids);
        #endregion

        #region Create
        Task<Guid> CreateAssync(T entity);

        Task CreateRangeAssync(IEnumerable<T> entities);
        #endregion

        #region Update
        Task<bool> TryUpdateAssync(T entity);
        #endregion

        #region Delete
        Task<bool> TryDeleteAssync(Guid id);
        Task DeleteRangeAsync(IEnumerable<Guid> ids);
        #endregion

        Task<bool> IsExistByIdAssync(Guid id);
    }
}